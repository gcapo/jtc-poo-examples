package ejemplos.polimorfismo;

/**
 * Clase C
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class C extends B {

    public void q() {
        System.out.println("C.q()");        
    }
    
    public void r() {
        q();
        p();
    }
}