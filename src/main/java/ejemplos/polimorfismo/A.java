package ejemplos.polimorfismo;

/**
 * Clase A
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class A {
    public void p() {
        System.out.println("A.p()");
    }
    
    public void q() {
        System.out.println("A.q()");        
    }
    
    public void r() {
        p();
        q();
    }
}