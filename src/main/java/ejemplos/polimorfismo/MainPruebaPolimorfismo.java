package ejemplos.polimorfismo;

/**
 * Clase TestPolimorfismo
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class MainPruebaPolimorfismo {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        A a = new B();
        a.r();
        a = new C();
        a.r();
        
        //Que imprimira el codigo de arriba?..

    } //Fin de main

} //Fin de clase TestHS
