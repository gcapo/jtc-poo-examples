package ejemplos.abstractas;

/**
 * Clase ProfesorEscalafonado
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class ProfesorContratado extends Profesor {

    @Override
    public void imprimir() {
        System.out.println("Impresion de profesor contratado");
    }

} //Fin de clase ProfesorEscalafonado
