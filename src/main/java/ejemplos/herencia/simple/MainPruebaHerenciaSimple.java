package ejemplos.herencia.simple;

/**
 * Clase TestHerencia
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class MainPruebaHerenciaSimple {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        //Una referencia a ClasePadre puede referenciar a instancias de ClasePadre
        ClasePadre p = new ClasePadre("padre");
        //O tambien puede referenciar a cualquiera de sus clases hijas
        ClasePadre primeraHija = new ClaseHija("hija 1");
        //Para crear instancias de hija
        ClaseHija segundaHija = new ClaseHija("hija 2");
        
        //Examinemos que tiene p.
        
        //Examinemos que tiene primeraHija.
        
        //Examinemos que tiene segundaHija.
        
        //Si queremos ejecutar cosas de ClaseHija, usando una referencia del tipo de la clase padre
        //debemos hacer un cast        
        ClaseHija otraReferencia = (ClaseHija) primeraHija;
        
        //Si examinamos los valores de las variables primeraHija y otraReferencia
        //vemos que tenemos un alias.. 2 variables apuntando al mismo objeto
        System.out.println("Mem de otraReferencia: " + otraReferencia);
        System.out.println("Mem de primeraHija: " + primeraHija);    
        
        ClaseNieta n = new ClaseNieta();
        //Examinemos que tiene n.
        

    } //Fin de main

} //Fin de clase TestHerencia

