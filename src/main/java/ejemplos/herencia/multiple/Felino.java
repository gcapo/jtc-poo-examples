package ejemplos.herencia.multiple;

/**
 * Clase Felino Simula herencia multiple extendiendo una clase e implementando
 * una interfaz Asi, el objeto Felino es un Mamifero y un Cuadrupedo a la vez,
 * porque puede ejecutar todos sus metodos
 *
 * @author Derlis
 */
public class Felino extends Mamifero implements Cuadrupedo {

    public void camina() {
        System.out.println("caminando");

    }

    public void corre() {
        System.out.println("corriendo");

    }

    public void salta() {
        System.out.println("saltando");

    }
}
