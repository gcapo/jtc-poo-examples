package ejemplos.herencia.multiple;

/**
 * Interfaz Cuadrupedo
 * Curso de Programacion Java
 *
 * @author Derlis Zarate 
 */
public interface Cuadrupedo {

    int nroPatas = 4;

    public void camina();

    public void corre();

    public void salta();
}
