package ejemplos.herencia.multiple;

public class MainPruebaHerenciaMultiple {

    /**
     * @param args
     */
    public static void main(String[] args) {

        Felino gato = new Felino();
        gato.tomaLeche();
        gato.camina();
        gato.corre();

    }
}
