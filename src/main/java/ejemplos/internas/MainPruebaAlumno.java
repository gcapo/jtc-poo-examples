package ejemplos.internas;

import ejemplos.internas.Alumno.Libro;

/**
 * Clase TestAlumno
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class MainPruebaAlumno {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        Alumno a = new Alumno("Jose Perez", "Asuncion", "Ingenieria");
        
        //El objeto libro se crea a partir de una instancia del objeto alumno, porque es una clase interna
        Libro l1 = a.new Libro("Introduccion a Matematica", "S. Liptzh", 1980);
        Libro l2 = a.new Libro("Matematica Avanzada", "Steinbrush", 1987);
        
        //Una vez creado el objeto ya puede usarse como cualquier otro objeto
        a.prestarNuevoLibro(l1);
        a.prestarNuevoLibro(l2);
        
        a.imprimirLibrosPrestados();

    } //Fin de main

} //Fin de clase TestAlumno
