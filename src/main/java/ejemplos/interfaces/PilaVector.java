package ejemplos.interfaces;

/**
 * Clase PilaVector
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class PilaVector implements InterfazPila {

    public void apilar(int nro) {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

    public int desapilar() {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

    public boolean esPilaLlena() {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

    public boolean esPilaVacia() {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

    public int getTamanhoActualPila() {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

} //Fin de clase PilaVector
