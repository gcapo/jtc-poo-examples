package ejemplos.interfaces;

/**
 * Clase PilaEnlazada
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class PilaEnlazada implements InterfazPila {
    
    //NodoPila raiz;

    public void apilar(int nro) {
        throw new UnsupportedOperationException("Pendiente de implementacion");
    }

    public int desapilar() {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

    public boolean esPilaLlena() {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

    public boolean esPilaVacia() {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

    public int getTamanhoActualPila() {
    	throw new UnsupportedOperationException("Pendiente de implementacion");
    }

} //Fin de clase PilaEnlazada
