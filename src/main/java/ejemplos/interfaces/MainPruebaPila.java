package ejemplos.interfaces;

/**
 * Clase TestPila
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class MainPruebaPila {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        InterfazPila p;
        
        p = new PilaEnlazada();
        
        p.apilar(4);
        p.apilar(5);
        p.desapilar();

    } //Fin de main

} //Fin de clase TestPila
